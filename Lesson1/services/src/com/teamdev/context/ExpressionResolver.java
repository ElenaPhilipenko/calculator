package com.teamdev.context;

public interface ExpressionResolver<ExpressionOperator, ExpressionOperand> {

    void addOperator(ExpressionOperator operator);

    void resolveOperator(ExpressionOperator operator);

    void addOperand(ExpressionOperand number);

    ExpressionOperand getResult();

    void addOpenedBracket();

    void addClosedBracket();

    void addComma();

}
