package com.teamdev.context;

import com.teamdev.provider.*;
import com.teamdev.stateMachine.StateMachineContext;

import java.util.LinkedList;

public class EvaluationContext implements StateMachineContext<Double> {

    private EvaluationResolver resolver;
    private final BinaryOperatorProvider binaryOperatorProvider = new BinaryOperatorProvider();
    private final FunctionProvider functionProvider = new FunctionProvider();
    private final LinkedList<EvaluationResolver> children = new LinkedList<EvaluationResolver>();

    public EvaluationContext(String expression) {
        resolver = new EvaluationResolver(new ExpressionReader(expression));
    }

    public BinaryOperatorProvider getBinaryOperatorProvider() {
        return binaryOperatorProvider;
    }

    public EvaluationResolver getResolver() {
        return resolver;
    }

    public FunctionProvider getFunctionProvider() {
        return functionProvider;
    }

    public ExpressionResolver createChild() {
        children.add(resolver);
        resolver = new EvaluationResolver(resolver.getReader());
        return resolver;
    }

    public void removeChild() {
        resolver = children.removeLast();
    }

    public int getAmountOfChildren(){
        return children.size();
    }

    @Override
    public Double getResult() {
        return resolver.getResult();
    }
}
