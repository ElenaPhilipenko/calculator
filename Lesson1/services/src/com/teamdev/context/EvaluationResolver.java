package com.teamdev.context;

import com.teamdev.exception.CalculateException;
import com.teamdev.operator.Operator;

import java.util.LinkedList;


public class EvaluationResolver implements ExpressionResolver<Operator, Double> {
    protected final LinkedList<Double> numbers;
    protected final LinkedList<Operator> operators;
    protected final LinkedList<Integer> nestedOperators;
    private int amountOfBrackets = 0;
    private final ExpressionReader reader;

    public EvaluationResolver(ExpressionReader reader) {
        this.reader = reader;
        this.numbers = new LinkedList<Double>();
        this.operators = new LinkedList<Operator>();
        this.nestedOperators = new LinkedList<Integer>();
    }

    public ExpressionReader getReader() {
        return reader;
    }

    public void addOperator(Operator operator) {
        resolveOperator(operator);
        operators.addLast(operator);
    }

    public void addOperand(Double number) {
        numbers.add(number);
    }

    public Double getResult() {
        if(amountOfBrackets>0){
            throw new CalculateException("Missed closed bracket", reader.getCursorPosition());
        }
        finishExpression();
        if (numbers.size() != 1) {
            throw new CalculateException("Wrong structure of expression");
        }
        return numbers.get(0);
    }

    public void addOpenedBracket() {
        nestedOperators.add(operators.size());
        ++amountOfBrackets;
    }

    public void resolveOperator(Operator operator) {
        if (!operators.isEmpty() && ((nestedOperators.isEmpty() || nestedOperators.getLast() < operators.size()))) {
            final int priorityPrev = operators.getLast().getPriority();
            final int priorityCurrent = operator.getPriority();
            if ((priorityPrev >= priorityCurrent && operator.isLeftAssociative())
                    || (priorityPrev > priorityCurrent && !operator.isLeftAssociative())) {
                numbers.addLast(operators.removeLast().execute(numbers));
            }
        }

    }

    public void addClosedBracket() {
        if(nestedOperators.isEmpty()){
            throw new CalculateException("Missed opened bracket");
        }
        Integer nested = nestedOperators.removeLast();
        while (operators.size() > nested) {
            numbers.addLast(operators.removeLast().execute(numbers));
        }
        --amountOfBrackets;
    }

    @Override
    public void addComma() {
        int nested = (!nestedOperators.isEmpty())? nestedOperators.getLast() : 0;
        while (operators.size() > nested) {
            numbers.addLast(operators.removeLast().execute(numbers));
        }
    }

    public void addFunction(Operator function) {
        operators.addLast(function);
    }

    private void finishExpression() {
        while (!operators.isEmpty()) {
            numbers.addLast(operators.removeLast().execute(numbers));
        }
    }

    public boolean isBracketsValid(){
        return (amountOfBrackets==0);
    }
}

