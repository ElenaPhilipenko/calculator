package com.teamdev.context;

public class ExpressionReader {

    private final String expression;
    private int cursorPosition;

    public ExpressionReader(String expression) {
        this.expression = expression;
    }

    public String getCurrentExpression() {
        return expression.substring(cursorPosition);
    }

    public int getCursorPosition() {
        return cursorPosition;
    }

    public void moveCursor(int index) {
        cursorPosition += index;
    }

    public void skipWhiteSpaces() {
        while (cursorPosition < expression.length() && Character.isWhitespace(expression.charAt(cursorPosition))) {
            cursorPosition++;
        }
    }
}
