package com.teamdev.provider;

import com.teamdev.operator.*;

import java.util.TreeMap;
import java.util.TreeSet;

public class BinaryOperatorProvider implements TokenProvider<Operator> {

    private final TreeMap<String, Operator> nameTable = new TreeMap<String, Operator>() {
        {
            put("+", new Plus(1, true));
            put("-", new Minus(1, true));
            put("*", new Multiply(2, true));
            put("/", new Divide(2, true));
            put("^", new Pow(3, false));
        }
    };

    @Override
    public TreeSet<String> find(String startsWith) {
        final TreeSet<String> representations = new TreeSet<String>();
        for (String name : nameTable.keySet()) {
            if (startsWith.startsWith(name)) {
                representations.add(name);
            }
        }
        return representations;
    }

    @Override
    public Operator get(String representation) {
        return nameTable.get(representation);
    }
}
