package com.teamdev.provider;

import com.teamdev.operator.*;

import java.util.TreeMap;
import java.util.TreeSet;

public class FunctionProvider implements TokenProvider<Operator> {
    private final TreeMap<String, Operator> nameTable = new TreeMap<String, Operator>() {
        {
            put("min", new Min(3, false));
            put("max", new Max(3, false));
            put("sum", new Sum(3, false));
            put("sqrt", new Sqrt(3, false));
        }
    };


    @Override
    public TreeSet<String> find(String startsWith) {
         TreeSet<String> representations = new TreeSet<String>();
        for (String name : nameTable.keySet()) {
            if (startsWith.startsWith(name)) {
                representations.add(name);
            }
        }
        return representations;
    }

    @Override
    public Operator get(String representation) {
         return nameTable.get(representation);
    }
}
