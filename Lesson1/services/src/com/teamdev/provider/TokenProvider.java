package com.teamdev.provider;

import java.util.TreeSet;

public interface TokenProvider<ResultObject> {

    public TreeSet<String> find(String startsWith);

    public ResultObject get(String representation);
}
