package com.teamdev.transition;

public enum State {
    START,
    NUMBER,
    BINARY_OPERATOR,
    OPENED_BRACKET,
    CLOSED_BRACKET,
    FUNCTION,
    COMMA,
    FINISH
}
