package com.teamdev.transition;


import com.teamdev.stateMachine.TransitionMatrix;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;


public class EvaluateMatrix implements TransitionMatrix<State> {

    private final Map<State, Set<State>> matrix =
            new EnumMap<State, Set<State>>(State.class) {{
                put(State.START, EnumSet.of(State.NUMBER, State.OPENED_BRACKET, State.FUNCTION));
                put(State.NUMBER, EnumSet.of(State.FINISH, State.BINARY_OPERATOR, State.CLOSED_BRACKET, State.COMMA));
                put(State.BINARY_OPERATOR, EnumSet.of(State.NUMBER, State.OPENED_BRACKET, State.FUNCTION));
                put(State.OPENED_BRACKET, EnumSet.of(State.NUMBER, State.OPENED_BRACKET, State.FUNCTION));
                put(State.CLOSED_BRACKET, EnumSet.of(State.FINISH, State.BINARY_OPERATOR, State.CLOSED_BRACKET, State.COMMA));
                put(State.FUNCTION, EnumSet.of(State.OPENED_BRACKET));
                put(State.COMMA, EnumSet.of(State.NUMBER, State.OPENED_BRACKET, State.FUNCTION));

            }};

    public State getStartState() {
        return State.START;
    }

    public State getFinishState() {
        return State.FINISH;
    }

    public Set<State> getPossibleTransitions(State state) {
        return matrix.get(state);
    }
}
