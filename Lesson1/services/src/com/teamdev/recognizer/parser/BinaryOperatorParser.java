package com.teamdev.recognizer.parser;

import com.teamdev.provider.*;
import com.teamdev.context.EvaluationContext;
import com.teamdev.context.ExpressionReader;

import java.util.TreeSet;


public class BinaryOperatorParser implements ExpressionParser {

    public boolean accept(EvaluationContext context) {
        final ExpressionReader reader = context.getResolver().getReader();
        reader.skipWhiteSpaces();
        final BinaryOperatorProvider provider = context.getBinaryOperatorProvider();
        final TreeSet<String> name = provider.find(reader.getCurrentExpression());
        if (name.isEmpty()) {
            return false;
        }
        context.getResolver().addOperator(provider.get(name.last()));
        reader.moveCursor(name.last().length());
        return true;
    }



}
