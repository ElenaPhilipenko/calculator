package com.teamdev.recognizer.parser;

import com.teamdev.context.EvaluationContext;
import com.teamdev.context.ExpressionReader;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Locale;

public class NumberParser implements ExpressionParser {

    @Override
    public boolean accept(EvaluationContext context) {

        final ExpressionReader reader = context.getResolver().getReader();
        reader.skipWhiteSpaces();
        final ParsePosition parsePosition = new ParsePosition(0);
        final NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
        numberFormat.setParseIntegerOnly(false);
        numberFormat.setGroupingUsed(false);
        final java.lang.Number number = numberFormat.parse(reader.getCurrentExpression(), parsePosition);
        if (number != null) {
            reader.moveCursor(parsePosition.getIndex());
            context.getResolver().addOperand(number.doubleValue());

        }
        return number != null;
    }
}
