package com.teamdev.recognizer.parser;

import com.teamdev.context.EvaluationContext;
import com.teamdev.context.ExpressionReader;

import java.util.TreeSet;

public class OpenedBracketParser implements ExpressionParser {

    private final TreeSet<String> openedBracket = new TreeSet<String>() {{
        add("(");
    }};

    @Override
    public boolean accept(EvaluationContext context) {
        final ExpressionReader reader = context.getResolver().getReader();
        reader.skipWhiteSpaces();
        for (String name : openedBracket) {
            if (reader.getCurrentExpression().startsWith(name)) {
                context.getResolver().addOpenedBracket();
                reader.moveCursor(name.length());
                return true;
            }
        }
        return false;
    }
}
