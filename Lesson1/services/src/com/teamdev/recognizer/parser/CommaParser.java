package com.teamdev.recognizer.parser;

import com.teamdev.context.EvaluationContext;
import com.teamdev.context.ExpressionReader;

import java.util.TreeSet;

public class CommaParser implements ExpressionParser {
    private final TreeSet<String> comma = new TreeSet<String>() {{
        add(",");
    }};

    @Override
    public boolean accept(EvaluationContext context) {
        final ExpressionReader reader = context.getResolver().getReader();
        reader.skipWhiteSpaces();
        for (String name : comma) {
            if (reader.getCurrentExpression().startsWith(name)) {
                context.getResolver().addComma();
                reader.moveCursor(name.length());
                return true;
            }
        }
        return false;
    }
}
