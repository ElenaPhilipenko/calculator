package com.teamdev.recognizer.parser;


import com.teamdev.context.EvaluationContext;
import com.teamdev.context.ExpressionReader;

public class EndOfExpressionValidator implements ExpressionParser {

    @Override
    public boolean accept(EvaluationContext context) {
        final ExpressionReader reader = context.getResolver().getReader();
        reader.skipWhiteSpaces();
        return reader.getCurrentExpression().isEmpty();
    }
}
