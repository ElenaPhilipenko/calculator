package com.teamdev.recognizer.parser;

import com.teamdev.context.EvaluationContext;
import com.teamdev.stateMachine.StateRecognizer;

public interface ExpressionParser extends StateRecognizer<EvaluationContext> {

    @Override
    boolean accept(EvaluationContext context);
}
