package com.teamdev.recognizer.parser;

import com.teamdev.context.EvaluationContext;
import com.teamdev.context.ExpressionReader;

import java.util.TreeSet;

public class ClosedBracketParser implements ExpressionParser {
    private final TreeSet<String> closedBracket = new TreeSet<String>() {{
        add(")");
    }};

    @Override
    public boolean accept(EvaluationContext context) {
        final ExpressionReader reader = context.getResolver().getReader();
        reader.skipWhiteSpaces();
        for (String name : closedBracket) {
            if (reader.getCurrentExpression().startsWith(name)) {
                context.getResolver().addClosedBracket();
                detectEndOfNestedExpression(context);
                reader.moveCursor(name.length());
                return true;
            }
        }
        return false;
    }

    private void detectEndOfNestedExpression(EvaluationContext context) {
        if (context.getAmountOfChildren() > 0 && context.getResolver().isBracketsValid()) {
            double res = context.getResolver().getResult();
            context.removeChild();
            context.getResolver().addOperand(res);
        }
    }
}

