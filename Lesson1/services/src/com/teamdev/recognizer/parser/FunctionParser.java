package com.teamdev.recognizer.parser;

import com.teamdev.context.EvaluationContext;
import com.teamdev.context.ExpressionReader;
import com.teamdev.provider.*;
import java.util.TreeSet;

public class FunctionParser implements ExpressionParser {

    @Override
    public boolean accept(EvaluationContext context) {
        final ExpressionReader reader = context.getResolver().getReader();
        reader.skipWhiteSpaces();
        final FunctionProvider provider = context.getFunctionProvider();
        TreeSet<String> name = provider.find(reader.getCurrentExpression());
        if (name.isEmpty()) {
            return false;
        }
        context.createChild();
        context.getResolver().addFunction(provider.get(name.last()));
        reader.moveCursor(name.last().length());
        return true;
    }
}
