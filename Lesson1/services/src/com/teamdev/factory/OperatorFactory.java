package com.teamdev.factory;

import com.teamdev.operator.Operator;

public interface OperatorFactory {

    Operator create(String name);

}
