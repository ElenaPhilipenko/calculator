package com.teamdev.stateMachine;

import java.util.Set;

public abstract class FiniteStateMachine<Result,
        State extends Enum,
        Matrix extends TransitionMatrix<State>,
        Context extends StateMachineContext<Result>,
        Recognizer extends StateRecognizer<Context>> {

    public Result run(Context context) {
        final Matrix matrix = getTransitionMatrix();
        State state = matrix.getStartState();

        while (state != matrix.getFinishState()) {
            state = moveForward(context, matrix.getPossibleTransitions(state));
            if (state == null) {
                deadlock(context);
            }
        }
        return context.getResult();
    }

    private State moveForward(Context context, Set<State> possibleTransitions) {
        for (State possibleState : possibleTransitions) {
            if (getStateRecognizer(possibleState).accept(context)) {
                return possibleState;
            }
        }
        return null;
    }

    protected abstract Matrix getTransitionMatrix();

    protected abstract Recognizer getStateRecognizer(State state);

    protected abstract void deadlock(Context context);
}
