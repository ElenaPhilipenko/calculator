package com.teamdev.stateMachine;

public interface StateRecognizer<Context extends StateMachineContext> {

    boolean accept(Context context);
}
