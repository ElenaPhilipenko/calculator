package com.teamdev.stateMachine;


public interface StateMachineContext<Result> {
    Result getResult();
}
