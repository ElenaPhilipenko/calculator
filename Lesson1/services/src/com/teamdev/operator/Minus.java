package com.teamdev.operator;

import java.util.LinkedList;

public class Minus extends Operator {

    public Minus(int priority, boolean leftAssociative) {
        super(priority, leftAssociative);
    }

    @Override
    public double execute(LinkedList<Double> args) {
        if (args.size() == 1) {
            return args.removeLast() * (-1);
        } else {
            final double b = args.removeLast();
            final double a = args.removeLast();
            return a - b;
        }
    }


}
