package com.teamdev.operator;

import java.util.LinkedList;

public class Min extends Operator {

    public Min(int priority, boolean leftAssociative) {
        super(priority, leftAssociative);
    }

    @Override
    public double execute(LinkedList<Double> args) {
        double minValue = args.get(0);
        while (!args.isEmpty()) {
            minValue = Math.min(args.removeLast(), minValue);
        }
        return minValue;
    }
}
