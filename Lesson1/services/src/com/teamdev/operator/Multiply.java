package com.teamdev.operator;

import java.util.LinkedList;

public class Multiply extends Operator {

    public Multiply(int priority, boolean leftAssociative) {
        super(priority, leftAssociative);
    }

    @Override
    public double execute(LinkedList<Double> args) {
        return args.removeLast() * (args.removeLast());
    }
}
