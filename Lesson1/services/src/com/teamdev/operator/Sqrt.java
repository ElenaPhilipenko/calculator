package com.teamdev.operator;

import java.util.LinkedList;

public class Sqrt extends Operator {
    public Sqrt(int priority, boolean leftAssociative) {
        super(priority, leftAssociative);
    }

    @Override
    public double execute(LinkedList<Double> args) {
        return Math.sqrt(args.removeLast());
    }
}