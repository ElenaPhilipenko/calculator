package com.teamdev.operator;

import java.util.LinkedList;

public class Max extends Operator {

    public Max(int priority, boolean leftAssociative) {
        super(priority, leftAssociative);
    }

    @Override
    public double execute(LinkedList<Double> args) {
        double maxValue = args.get(0);
        while (!args.isEmpty()) {
            maxValue = Math.max(args.removeLast(), maxValue);
        }
        return maxValue;

    }
}
