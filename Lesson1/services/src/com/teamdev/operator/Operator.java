package com.teamdev.operator;

import java.util.LinkedList;

public abstract class Operator {
    private final int priority;
    private final boolean leftAssociative;

    public Operator(int priority, boolean leftAssociative) {
        this.priority = priority;
        this.leftAssociative = leftAssociative;
    }

    public int getPriority() {
        return priority;
    }

    public boolean isLeftAssociative() {
        return leftAssociative;
    }

    public abstract double execute(LinkedList<Double> args);


}
