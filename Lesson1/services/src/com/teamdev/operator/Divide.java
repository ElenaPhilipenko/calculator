package com.teamdev.operator;

import java.util.LinkedList;


public class Divide extends Operator {

    public Divide(int priority, boolean leftAssociative) {
        super(priority, leftAssociative);
    }

    @Override
    public double execute(LinkedList<Double> args) {
        final double b = args.removeLast();
        final double a = args.removeLast();
        return a / b;
    }

}
