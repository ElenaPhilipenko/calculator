package com.teamdev.operator;

import java.util.LinkedList;

public class Plus extends Operator {

    public Plus(int priority, boolean leftAssociative) {
        super(priority, leftAssociative);
    }

    @Override
    public double execute(LinkedList<Double> args) {
        return args.removeLast() + (args.removeLast());
    }


}
