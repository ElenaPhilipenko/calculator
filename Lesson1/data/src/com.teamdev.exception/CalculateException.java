package com.teamdev.exception;

public class CalculateException extends RuntimeException {
    private final int position;

    public CalculateException() {
        position = 0;
    }

    public CalculateException(String message) {
        super(message);
        position = 0;
    }

    public CalculateException(String message, int position) {
        super(message);
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}