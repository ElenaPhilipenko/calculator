package tests;

import com.teamdev.calculator.CalculatorFiniteStateMachine;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    ////////////////plus//////////////////
    CalculatorFiniteStateMachine c = new CalculatorFiniteStateMachine();

    @Test
    public void testPlus() throws Exception {
        assertEquals(1d, c.calculate("1+0"), 0);
    }

    @Test
    public void testNegativePlus() throws Exception {
        assertEquals(3d, c.calculate("-10+13"), 0);
    }

    @Test
    public void testPlusNegative() throws Exception {
        assertEquals(-23, c.calculate("-10+-13"), 0);
    }

    @Test
    public void testDoublePlus() throws Exception {
        assertEquals(9.9, c.calculate("6.9+3"), 0);
    }

    ////////////////divide////////////////////////////
    @Test
    public void testDoubleDivide() throws Exception {
        assertEquals(5.217391304347826d, c.calculate("36/6.9"), 0);
    }

    @Test
    public void testDivide() throws Exception {
        assertEquals(0.5d, c.calculate("1/2"), 0);
    }

    @Test
    public void testDividingZero() throws Exception {
        assertEquals(0d, c.calculate("0/2"), 0);
    }

    @Test
    public void testUnaryMinusWithDivide() throws Exception {
        assertEquals(-3d, c.calculate("6/-2"), 0);
    }
    ////////////////minus//////////////////////////

    @Test
    public void testMinusNegate() throws Exception {
        assertEquals(-4d, c.calculate("-9-(-5)"), 0);
    }

    ////////////////parse/////////////////////////
    @Test
    public void testParseNumber() throws Exception {
        assertEquals(42d, c.calculate("42"), 0);
    }

    @Test
    public void testParseNegativeNumber() throws Exception {
        assertEquals(-42d, c.calculate("-42"), 0);
    }

    ////////////////power///////////////////////
    @Test
    public void testPowZero() throws Exception {
        assertEquals(0d, c.calculate("0^2"), 0);
    }

    @Test
    public void testPow() throws Exception {
        assertEquals(4d, c.calculate("2^2"), 0);
    }

    @Test
    public void testPowInPairPower() throws Exception {
        assertEquals(4d, c.calculate("(-2)^2"), 0);
    }

    @Test
    public void testPowInNotPairPower() throws Exception {
        assertEquals(-8d, c.calculate("-2^3"), 0);
    }

    @Test
    public void powerTest() throws Exception {
        assertEquals(1, c.calculate("1^0"), 0);
    }

    @Test
    public void testNegativeInZeroPower() throws Exception {
        assertEquals(1, c.calculate("-1^0"), 0);
    }

    @Test
    public void testNegativePower() throws Exception {
        assertEquals(1, c.calculate("1^(-2)"), 0);
    }

    @Test
    public void testUnaryMinusWithPow() throws Exception {
        assertEquals(0.015625d, c.calculate("8^-2"), 0);
    }


    ///////////////multiply///////////////////////
    @Test
    public void testMultiplyNegative() throws Exception {
        assertEquals(-6d, c.calculate("-2*3"), 0);
    }

    @Test
    public void testMultiplyNegativeDouble() throws Exception {
        assertEquals(-1.5d, c.calculate("0.5*(-3)"), 0);
    }

    @Test
    public void testMultiplyTwoNegative() throws Exception {
        assertEquals(6d, c.calculate("-2*(-3)"), 0);
    }

    @Test
    public void testUnaryMinusWithMultiply() throws Exception {
        assertEquals(6d, c.calculate("-3*-2"), 0);
    }

    @Test
    public void testNegativeDoubleMultiply() throws Exception {
        assertEquals(-0.000036d, c.calculate("-0.00006*0.6"), 0);
    }

    //////////////////a lot of brackets///////////////////////////
    @Test
    public void testBrackets() throws Exception {
        assertEquals(-1d, c.calculate("(1+2)-(1+1)*(2-0)"), 0);
    }

    @Test
    public void testBrackets1() throws Exception {
        assertEquals(-14d, c.calculate("(((1+2)-(1+1)*(2-0)+2)*7)+6+(9-9+(8-(8-9)-8))"), 0);
    }

    //////////////////functions//////////////////////////////
    @Test
    public void testMin() throws Exception {
        assertEquals(-3d, c.calculate("min(12,-3,2,3,5)"), 0);
    }

    @Test
    public void testMax() throws Exception {
        assertEquals(662d, c.calculate("max(12,222,332,662,2)"), 0);
    }

    @Test
    public void testSqrt() throws Exception {
        assertEquals(19d, c.calculate("sqrt(361)"), 0);
    }

    @Test
    public void testLongExpression() {
        assertEquals(19.62d, c.calculate("(sqrt(361)-(12-(2^min(2))+12/6-0.9^(max(2))))*2"), 0);
    }

    @Test
    public void testSum() {
        assertEquals(6d, c.calculate("sum(2,-2,2,2,2)"), 0);
    }

    //////////////////nested functions////////////////////////
    @Test
    public void testNestedOperatorsInSum() throws Exception {
        assertEquals(6d, c.calculate("sum(2,-2,2,2-2,2+2)"), 0);
    }

    @Test
    public void testNestedFunctions() throws Exception {
        assertEquals(8d, c.calculate("sum(2,min(2,1+max(9,((((1+2)-(1+1)*(2-0)+2)*7)+6+(9-9+(8-(8-9)-8))-12),2,2,5,0)),2-2,sqrt(8+8))"), 0);
    }

    @Test
    public void testNestedOperatorsInMax() throws Exception {
        assertEquals(4d, c.calculate("max(20/10,(-2  *(12+8-2*5))/10,2,2-2,2+2)"), 0);
    }
}
