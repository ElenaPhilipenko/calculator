package tests;

import com.teamdev.calculator.CalculatorFiniteStateMachine;
import com.teamdev.exception.CalculateException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TestErrors {
    CalculatorFiniteStateMachine c = new CalculatorFiniteStateMachine();

    @Test
    public void testOperatorOneByOne() {
        try {
            c.calculate("7+-");
        } catch (Exception ce) {
            assertEquals("Wrong element at position 2", ce.getMessage());
        }
    }

    @Test
    public void testMissedBracketForFunction() {
        try {
            c.calculate("min");
        } catch (Exception ce) {
            assertEquals("Wrong element at position 3", ce.getMessage());
        }
    }

    @Test
    public void testUnknownElement() throws Exception {
        try {
            c.calculate("k");
        } catch (CalculateException e) {
            assertEquals("Wrong element at position 0", e.getMessage());
        }
    }


    @Test
    public void testEmptyExpression() {
        try {
            c.calculate("");
        } catch (Exception ce) {
            assertEquals("Empty expression", ce.getMessage());
        }
    }

    @Test
    public void testWhiteSpace() {
        try {
            c.calculate("         ");
        } catch (Exception ce) {
            assertEquals("Empty expression", ce.getMessage());
        }
    }

    @Test
    public void testMultiplePoint() throws Exception {
        try {
            c.calculate("2.0.0");
        } catch (Exception e) {
            assertEquals("Wrong element at position 3", e.getMessage());
        }
    }

    @Test
    public void testInfinity() throws Exception {
        CalculatorFiniteStateMachine c = new CalculatorFiniteStateMachine();
        try {
            c.calculate("23333333333333388^4^5");
        } catch (Exception e) {
            assertEquals("Infinity", e.getMessage());
        }
    }

    @Test
    public void testMissOpenBracket() throws Exception {
        try {
            c.calculate("(2^4)^5)");
        } catch (Exception e) {
            assertEquals("Missed opened bracket", e.getMessage());
        }
    }

    @Test
    public void testMissCloseBracket() throws Exception {
        try {
            c.calculate("2^4*5-8)");
        } catch (Exception e) {
            assertEquals("Missed opened bracket", e.getMessage());
        }
    }


    @Test
    public void testWrongArgsForSqrt() throws Exception {
        try {
            c.calculate("sqrt(1,2");

        } catch (CalculateException e) {
            assertEquals("Missed closed bracket", e.getMessage());
        }
    }

    @Test
    public void testNoArgs() throws Exception {
        CalculatorFiniteStateMachine c = new CalculatorFiniteStateMachine();
        try {
            c.calculate("sqrt()");

        } catch (CalculateException e) {
            assertEquals("Wrong element at position 5", e.getMessage());
        }
    }

    @Test
    public void testNoCloseBracketForFunction() throws Exception {
        try {
            c.calculate("sqrt(1.9");

        } catch (CalculateException e) {
            assertEquals("Missed closed bracket", e.getMessage());
        }
    }

    @Test
    public void testNoCloseBracket() throws Exception {
        try {
            c.calculate("(-12+9");

        } catch (CalculateException e) {
            assertEquals("Missed closed bracket", e.getMessage());
        }
    }

    @Test
    public void testMissedOperator() throws Exception {
        try {
            c.calculate("-12(9");

        } catch (CalculateException e) {
            assertEquals("Wrong element at position 3", e.getMessage());
        }
    }


    @Test
    public void testSeparator() throws Exception {
        try {
            c.calculate("min(2+9, 9),");

        } catch (CalculateException e) {
            assertEquals("Wrong element at position 12", e.getMessage());
        }
    }

}