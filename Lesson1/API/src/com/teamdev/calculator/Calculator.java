package com.teamdev.calculator;

import com.teamdev.exception.CalculateException;

public interface Calculator {

    public Double calculate(String expression) throws CalculateException;
}
