package com.teamdev.calculator;

import com.teamdev.recognizer.parser.BinaryOperatorParser;
import com.teamdev.context.EvaluationContext;
import com.teamdev.context.ExpressionReader;
import com.teamdev.exception.CalculateException;
import com.teamdev.recognizer.parser.*;
import com.teamdev.stateMachine.FiniteStateMachine;
import com.teamdev.transition.EvaluateMatrix;
import com.teamdev.transition.State;

import java.util.HashMap;
import java.util.Map;


public class CalculatorFiniteStateMachine  extends FiniteStateMachine<Double,
        State, EvaluateMatrix, EvaluationContext, ExpressionParser> implements Calculator{

    private final Map<State, ExpressionParser> parsers =
            new HashMap<State, ExpressionParser>() {{
                put(State.NUMBER, new NumberParser());
                put(State.FINISH, new EndOfExpressionValidator());
                put(State.BINARY_OPERATOR, new BinaryOperatorParser());
                put(State.OPENED_BRACKET, new OpenedBracketParser());
                put(State.CLOSED_BRACKET, new ClosedBracketParser());
                put(State.COMMA, new CommaParser());
                put(State.FUNCTION, new FunctionParser());
            }};


    private final EvaluateMatrix matrix = new EvaluateMatrix();

    @Override
    protected EvaluateMatrix getTransitionMatrix() {
        return matrix;
    }

    @Override
    protected ExpressionParser getStateRecognizer(State evaluationState) {
        return parsers.get(evaluationState);
    }

    @Override
    protected void deadlock(EvaluationContext evaluationContext) {
        int position = evaluationContext.getResolver().getReader().getCursorPosition();
        throw new CalculateException("Wrong element " + "at position " + position, position);
    }

    @Override
    public Double calculate(String expression) throws CalculateException{
        EvaluationContext context = new EvaluationContext(expression);
        ExpressionReader reader = context.getResolver().getReader();
        reader.skipWhiteSpaces();
        if(reader.getCurrentExpression().isEmpty()){
            throw new CalculateException("Empty expression");
        }
        return run(new EvaluationContext(expression));
    }

}
