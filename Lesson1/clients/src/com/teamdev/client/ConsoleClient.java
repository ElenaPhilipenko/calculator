package com.teamdev.client;

import com.teamdev.calculator.CalculatorFiniteStateMachine;
import com.teamdev.exception.CalculateException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleClient {
    public static void openConsole() {
        String currentLine = ""; // Line read from standard in
        final CalculatorFiniteStateMachine calculator = new CalculatorFiniteStateMachine();
        System.out.println("Enter an expression (type 'exit' to exit): ");
        InputStreamReader converter = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(converter);
        while (!(currentLine.equals("exit"))) {
            try {
                currentLine = in.readLine();
            } catch (IOException e) {
                throw new CalculateException();
            }
            if (currentLine.equals("calc")) {
            } else if (!(currentLine.equals("exit"))) {
                try {
                    System.out.println("= " + calculator.calculate(currentLine));
                } catch (CalculateException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        System.exit(0);
    }
}
