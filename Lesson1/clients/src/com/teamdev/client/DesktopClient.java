package com.teamdev.client;

import com.teamdev.calculator.CalculatorFiniteStateMachine;
import com.teamdev.exception.CalculateException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class DesktopClient extends JFrame {
    private static final int HEIGHT = 140;
    private static final int WIDTH = 500;
    private JTextField expression;
    private JTextField answer;
    private final static JLabel jLabel = new JLabel("Expression:");


    public DesktopClient() {
        super("Calculator");
        setSize(WIDTH, HEIGHT);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        JPanel panel = new JPanel();

        expression = new JTextField("", 38);
        answer = new JTextField("", 38);
        JButton buttonEqual = new JButton("=");
        JButton buttonClear = new JButton("C");

        ActionListener equalsListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getAnswer();
            }
        };

        buttonEqual.addActionListener(equalsListener);

        buttonClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                answer.setText("");
                expression.setText("");
            }
        });

        KeyListener hideFormListener = new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_H) {
                    setVisible(false);
                    ConsoleClient.openConsole();

                } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    getAnswer();
                }
            }

            public void keyReleased(KeyEvent e) {
            }
        };
        expression.addKeyListener(hideFormListener);
        panel.add(jLabel);
        panel.add(expression);
        expression.requestFocus();
        panel.add(buttonEqual);
        panel.add(buttonClear);
        panel.add(answer);
        contentPane.add(panel);
    }

    private void getAnswer() {
        CalculatorFiniteStateMachine calculator = new CalculatorFiniteStateMachine();
        answer.setText("");
        try {
            answer.setText(calculator.calculate(expression.getText()).toString());
        } catch (CalculateException calcException) {
            answer.setText(calcException.getMessage());
            expression.requestFocus();
            expression.setCaretPosition(calcException.getPosition());
        } catch (Exception exception) {
            answer.setText(exception.getMessage());
        }
    }

    public static void main(String[] args) {
        final DesktopClient gui = new DesktopClient();
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setVisible(true);
    }
}
